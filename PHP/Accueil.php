<?php
session_start();
require 'Configure.php';
$numUser=$_SESSION['numCo'];
?>


<html>
	
	<head>
		<title>LinkedInOob</title>
		<link href="Css/ToolBar.css" rel="stylesheet" type="text/css" />
		<link href="Css/Accueil.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="Js/goToProfil.js"></script>
	</head>

	<body>
  
		<div class="container">

			<div class="topnav">
				<table>
				<tr>
					<td><h1>LinkedInOob<h1></td>
					<td>
					<a class="active" href="Accueil.php">Accueil</a>
					<a href="MonProfil.php">Mon Profil</a>
					<a href="MonReseau.php">Mon Réseau</a>
					<a href="Notifications.php">Notifications</a>
					<a href="Emplois.php">Emplois</a>
					</td>
				</tr>
				</table>
			</div>
	
			<div class="contLeft">
			<?php
			if($db_found)
			{
				$sql="SELECT profil.numuser, profil.photoprofil, profil.prenom, profil.nom FROM profil, ami WHERE ami.numuser='$numUser' AND ami.numami=profil.numuser ";
				$result=mysqli_query($db_handle, $sql);
				while($data=mysqli_fetch_assoc($result))
				{
					echo"<table>
					<tr>
						<td>
						<img class=\"photoProfil\" src=\"  ".$data['photoprofil']." \" onclick=\"goToProfil()\"/>
						</td>
						<td><a href='ProfilAmi.php?ami=".$data['numuser']."'>".$data['prenom']." ".$data['nom']."</a></td>
					</tr>
					</table>";
				}
			}
			?>			
			</div>
			
			<div class="contRight">
			
			<?php
			if ($db_found)
			{
				$sql="SELECT DISTINCT evenement.photo, evenement.description, evenement.dateevent, evenement.numevenement
				FROM evenement, ami, inscription
				WHERE ami.numuser='$numUser'
				AND inscription.numuser=ami.numami
				AND inscription.numevent=evenement.numevenement
				ORDER BY evenement.dateevent DESC";
				$result=mysqli_query($db_handle, $sql);
				while($data=mysqli_fetch_assoc($result))
				{
					
						echo"<table class=\"publication\">
						<tr>
							<td><p>".$data['description']."</p></td>
						</tr>
						<tr>
							<td><img class=\"post\" src=\"".$data['photo']."\"/></td>
						</tr>
						<tr>
							<td><p>Date de l'event : ".$data['dateevent']."</p></td>
						</tr>";
					
					
					echo"
					<tr>
						<td><form action=\"insertcomment1.php\" method=\"post\">							  
							  <label for=\"Comment\"><b>Comment   :</b></label>
								<input type=\"text\" placeholder=\"Enter Comment\" name=\"comment\">
								<input name=\"test\" type=\"hidden\" value=\"".$data['numevenement']."\">
							 </form>
						
						<td>
					</tr>
					</table>
					</br>";
				}
			}
			?>
			
			<?php
			if ($db_found)
			{
				$sql="SELECT DISTINCT post.numpost, post.contenue, post.type, post.visibilite, profil.prenom, profil.nom FROM profil, ami, post WHERE (post.visibilite='all' AND post.numuser!='$numUser')AND post.numuser=profil.numuser Or (ami.numuser='$numUser' AND profil.numuser=ami.numami AND post.visibilite='ami' AND post.numuser=ami.numami)";
				$result=mysqli_query($db_handle, $sql);
				while($data=mysqli_fetch_assoc($result))
				{
					if($data['type']=="commentaire")
					{
						echo"<table class=\"publication\">
						<tr>
							<td>
							<p>".$data['contenue']."</p>
							</td>
						</tr>";
					}
					
					if($data['type']=="photo")
					{
						echo"<table class=\"publication\">
						<tr>
							<td>
							<img class=\"post\" src=\"".$data['contenue']."\"/>
							</td>
						</tr>";
					}
					
					if($data['type']=="video")
					{
						echo"<table class=\"publication\">
						<tr>
							<td>
							<video width=\"400\" controls>
								<source src=\"  ".$data['contenue']." \" type=\"video/mp4\">
								Your browser does not support HTML5 video.
							</video>
							</td>
						</tr>";
					}
					
					echo"
					<tr>
						<td>Posté par: ".$data['prenom']." ".$data['nom']."</td>
					</tr>
					<tr>
						<td>";
						
					$sql2="SELECT COUNT(numpost) AS nb FROM reaction where numpost=".$data['numpost']."";
					$result2=mysqli_query($db_handle, $sql2);
					while($data2=mysqli_fetch_assoc($result2))
					{
						echo"
						<a href='like.php?numpost=".$data['numpost']."'><input type=\"button\" value=\"Like\"></a>"."  ".$data2['nb']."
						<a href='Share.php?numpost=".$data['numpost']."'><input type=\"button\" value=\"Share\"></a>
						<input type=\"button\" value=\"Comment\">
						</td>
					</tr>
					</table>
					</br>";
					}
				}
			}
			?>
			</div>
				
		</div>

	</body>
	
	<footer>
		<br>
		<p>Created by 
		<a href="remi.motsch@edu.ece.fr">MOTSCH Remi</a>,
		<a href="hugo.joseph-antoine@edu.ece.fr">JOSEPH-ANTOINE Hugo</a>,
		<a href="jg151954@edu.ece.fr">GIRET-IMHAUS Joaquim</a></p>
		<p>Copyright &copy; 2018<p>
	</footer>

</html>
